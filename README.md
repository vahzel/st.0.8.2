st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.


Requirements
------------
In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

Extras
------
Build include this patches:
* st-alpha-0.8.2.diff
* st-clipboard-0.8.2.diff,
* st-ligatures-alpha-20200428-28ad288.diff
* st-newterm-0.8.2.diff, 
* st-nordtheme-0.8.2.diff

Default terminal  font 
[Iosevka SS09](https://github.com/be5invis/Iosevka/releases/download/v3.3.1/ttf-iosevka-term-ss09-3.3.1.zip)

    wget https://github.com/be5invis/Iosevka/releases/download/v3.3.1/ttf-iosevka-term-ss09-3.3.1.zip ;
    mkdir -p ~/.local/share/fonts ;  unzip -jW ttf-iosevka-term-ss09-3.3.1.zip "ttf/*.ttf"  -d ~/.local/share/fonts/iosevka/ 





